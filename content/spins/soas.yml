title: Fedora Sugar on a Stick Spin
description: >-
  Sugar on a Stick is a Fedora-based operating system featuring the award-winning Sugar Learning Platform and designed to fit on an ordinary USB thumbdrive ("stick").


  Sugar sets aside the traditional “office-desktop” metaphor, presenting a child-friendly graphical environment. Sugar automatically saves your progress to a "Journal" on your stick, so teachers and parents can easily pull up "all collaborative web browsing sessions done in the past week" or "papers written with Daniel and Sarah in the last 24 hours" with a simple query rather than memorizing complex file/folder structures. Applications in Sugar are known as Activities, some of which are described below.


  It is now deployable for the cost of a stick rather than a laptop; students can take their Sugar on a Stick thumbdrive to any machine - at school, at home, at a library or community center - and boot their customized computing environment without touching the host machine’s hard disk or existing system at all.
image:
  src: https://stg.fedoraproject.org/assets/images/spins/spins-soas-dark.png
  alt: Fedora SOAS Image
  width: 400
  height: 300
links:
  - text: Download Now
    url: /spins/soas/download/
screenshot_image:
  image: public/assets/images/spins/screenshot-soas.jpg
  alt_text: Screenshot of Fedora SOAS desktop
sections:
  - sectionTitle: Exploring the wider world
    content:
      - title: Browse
        description: Access the internet with Browse Activity, bookmark sites to research with friends and save sessions to keep organized.
        image: public/assets/images/spins/soas_web.png
      - title: Get Books
        description: Download electronic books from all over the web. Explore with ease the classics and modern books from the Internet Archive, Feedbooks, etc.
        image: public/assets/images/spins/soas_books.png
      - title: Read
        description: Explore reports, documents, books and comics with the Read Activity.
        image: public/assets/images/spins/soas_read.png
      - title: Chat
        description: Chat provides a simple interface for collaborative discussion between two individuals or among a group as large as an entire classroom.
        image: public/assets/images/spins/soas_chat.png
  - sectionTitle: Reflect on what you've learned
    content:
      - title: Journal
        description:
          The Journal provides an interface into a datastore of everything you've created in Sugar.
          Bookmarking and commenting tools integrate with the Journal to allow parents and teachers to review a child's learning progress.
        image: public/assets/images/spins/soas_journal.png
      - title: Portfolio
        description: Portfolio provides a means to create periodic snapshots in slideshow form of representative work that shows what the learner can do.
        image: public/assets/images/spins/soas_portfolio.png
  - sectionTitle: Getting technical
    content:
      - title: Turtle Blocks
        description: Learn programming concepts with snap together blocks. Create art, animations and interactive programs in a graphics focused environment.
        image: public/assets/images/spins/soas_turtleart.png
      - title: Pippy
        description: Program applications in a simple environment. The Python backend provides unlimited opportunities within a simple language.
        image: public/assets/images/spins/soas_pippy.png
  - sectionTitle: Multimedia
    content:
      - title: Jukebox
        description: Jukebox is a simple media player to consume different kinds of audio and video files as well as your own creations.
        image: public/assets/images/spins/soas_jukebox.png
      - title: Paint
        description: Paint provides the tools to make artistic creations. Use brushes, stamps, shapes, text and images to create beautiful pictures.
        image: public/assets/images/spins/soas_paint.png
      - title: Record
        description: Record is a basic rich-media capture Activity. It lets you capture still images, video, and/or audio.
        image: public/assets/images/spins/soas_record.png
      - title: Imageviewer
        description: A simple and fast image viewer tool for Sugar. It has standard features, like zoom and rotate.
        image: public/assets/images/spins/soas_imageviewer.png
  - sectionTitle: Understanding and creating content
    content:
      - title: Write
        description: Create a story, poem, report or anything with Write. Use formatting tools to add images or colors and work together with friends.
        image: public/assets/images/spins/soas_write.png
      - title: Labyrinth
        description: Paint provides the tools to make artistic creations. Use brushes, stamps, shapes, text and images to create beautiful pictures.
        image: public/assets/images/spins/soas_labyrinth.png
      - title: Physics
        description: Create real life simulations using shapes, motors, ropes and bolts to explore physics in the world.
        image: public/assets/images/spins/soas_physics.png
      - title: FotoToon
        description: Use images and text to create comic strips. FotoToon provides many options to add motion, speech and thought to creations.
        image: public/assets/images/spins/soas_fototoon.png

downloadSection:
  description:
    We're so glad you've decided to give Fedora Sugar on a Stick Spin a try. We
    know you'll love it.
  links: []
